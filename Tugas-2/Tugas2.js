var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log("Jawaban Nomer 1 = "); 
console.log(word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+seventh); 

console.log("======================================== ");
var sentence = "I am going to be React Native Developer"; 
var firstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3];
var thirdWord = sentence[4] + sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22];
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]; 
var eighthWord = sentence[30]+ sentence[31] + sentence[32] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log("Jawaban Nomer 2");
console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)
console.log("======================================== ");

var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 15); 
var thirdWord2 = sentence2.substring(15, 17); 
var fourthWord2 = sentence2.substring(18, 20); 
var fifthWord2 = sentence2.substring(21, 25);  

console.log("Jawaban Nomer 3");
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
console.log("======================================== ");

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence2.substring(4, 15); 
var thirdWord3 = sentence2.substring(15, 17); 
var fourthWord3 = sentence2.substring(18, 20); 
var fifthWord3 = sentence2.substring(21, 25);

var firstWordLength = exampleFirstWord3.length 
var a = secondWord3.length
var b = thirdWord3.length
var c = fourthWord3.length
var d = fifthWord3.length

console.log("Jawaban Nomer 4");
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + a); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + b); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + c); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + d); 
console.log("======================================== ");

console.log("Tugas Conditional");
console.log("======================================== ");
console.log("if-else ");

var nama = ""
var nama1 = "John"
var peran1 = ""
var nama2 = "Jane"
var peran2 = "penyihir!"
var nama3 = "Jenita"
var peran3 = "Guard"
var nama4 = "Junaedi"
var peran4 = "werewolf!"

console.log("Nama harus diisi!");
if (nama == "") {
} if( nama1 == "John" || peran1=="") {
    console.log("Halo John, Pilih peranmu untuk memulai game!")
    
} if( nama2 == "Jane" || peran2 == "penyihir") {
    console.log("Selamat datang di Dunia Werewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")

} if( nama3 == "Jenita" || peran3 == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")

} if( nama4 == "Junaedi" || peran4 == "werewolf!"){
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")

} else {
    console.log("Nama harus diisi !");
}
console.log("======================================== ");
console.log("Switch Case");

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
switch(bulan) {
  case 1:   { namabulan ='Januari'; break; }
  case 2:   { naamabulan ='Febuari'; break; }
  case 3:   { namabulan ='Maret'; break; }
  case 4:   { naamabulan = 'April'; break; }
  case 5:   { naamabulan = 'Mei'; break; }
  case 6:   { naamabulan = 'Juni'; break; }
  case 7:   { naamabulan = 'Juli'; break; }
  case 8:   { naamabulan = 'Agustus'; break; }
  case 9:   { naamabulan = 'September'; break; }
  case 10:   { naamabulan = 'Oktober'; break; }
  case 11:   { naamabulan = 'November'; break; }
  case 12:   { naamabulan = 'Desemeber'; break; }
  default:  { console.log('Pilih Bulan'); }}

  console.log( hari+" "+ namabulan+" "+tahun); 
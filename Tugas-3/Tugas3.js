console.log('No. 1 Looping While '); 
console.log('===================='); 
var flag = 2;
var flag2 =20;
var nambah=2;
console.log('LOOPING PERTAMA'); 
while(flag <= 20) { 
  console.log(flag + " - I love coding ");
  nambah=2;
  flag+= nambah;
}
console.log('LOOPING KEDUA'); 
while(flag2 >= 2) {
  console.log(flag2 + " - I love coding ");
  flag2 -= nambah;
}
console.log('=============================='); 
console.log('No. 2 Looping menggunakan for '); 
console.log('=============================='); 
for(var angka = 1; angka <= 20; angka ++) {
  if (angka %2 != 0 && angka %3 !=0){
    console.log(angka + " - Santai");
  }
  if (angka %2 == 0 ){
    console.log(angka + " - Berkualitas");
  }
  if (angka %3 == 0 && angka % 2 !=0 ){
    console.log(angka + " - I love coding");
  } 
}

console.log('=============================='); 
console.log('No. 3 Membuat Persegi Panjang '); 
console.log('=============================='); 
var deret = 5;
var jumlah = 0;
while(deret > 0) {
  jumlah += deret; 
  deret--; 
  console.log('########')
}

console.log('==============================');
console.log('No. 4 Membuat Tangga '); 
console.log('=============================='); 
for(var angka = 0; angka <=7; angka ++) {
  if (angka = 1){
    console.log(" #");
  }
  if (angka = 2){
    console.log(" # #");
  }
  if (angka = 3){
    console.log(" # # #");
  }
   if (angka = 4){
    console.log(" # # # #");
  }
  if (angka = 5){
    console.log(" # # # # #");
  }
  if (angka = 6){
    console.log(" # # # # # #");
  }
  if (angka = 7){
    console.log(" # # # # # # #");
  }
}

console.log('=============================='); 
console.log('No. 5 Membuat Papan Catur '); 
console.log('=============================='); 
for(var angka = 2; angka <8; angka ++) {
  if (angka %2 != 0){
    console.log("# # # # #");
  }
  if (angka %3 != 0){
    console.log(" # # # # #");
  }
}
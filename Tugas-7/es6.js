//==================No 1 Mengubah fungsi menjadi fungsi arrow =====================
// before
// const golden = function goldenFunction(){
//     console.log("this is golden!!")
//   }
   
//   golden()
// after
goldenFunction = () => {
    console.log("this is golden!!")
  }

console.log("===== No 1 =====")
goldenFunction()

//==================No 2 Sederhanakan menjadi Object literal di ES6 =====================
// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//         return 
//       }
//     }
//   }
   
//   //Driver Code 
//   newFunction("William", "Imoh").fullName()

literal = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  console.log("===== No 2 =====")
  literal("William", "Imoh").fullName()

//==================No 3 Destructuring =====================
// const newObject = {
//     firstName: "Harry",
//     lastName: "Potter Holt",
//     destination: "Hogwarts React Conf",
//     occupation: "Deve-wizard Avocado",
//     spell: "Vimulus Renderus!!!"
//   }
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
  const { firstName, lastName,destination,occupation,spell } = newObject;
  console.log("===== No 3 =====")
  console.log(firstName, lastName, destination, occupation)

//==================No 4 Array Spreading =====================
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)

let combinedArray = [...west, ...east]
console.log(combinedArray)

//==================No 5 Template Literals =====================
const planet = "earth"
const view = "glass"
let before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
//console.log(before) 
const theString = {before}
console.log(theString) 